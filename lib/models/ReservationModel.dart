class ReservationModel {
  String _id;
  String _restaurantId;
  String _userId;
  String _seatNumber;
  String _reservationDate;

  ReservationModel(this._id,this._restaurantId, this._userId, this._seatNumber, this._reservationDate);

  ReservationModel.fromMap(dynamic obj) {
    this._id = obj['id'];
    this._restaurantId = obj['restaurant_id'];
    this._userId = obj['user_id'];
    this._seatNumber = obj['seat_number'];
    this._reservationDate = obj['reservation_date'];
  }

  String get id => _id;
  String get restaurantId => _restaurantId;
  String get userId => _userId;
  String get seatNumber => _seatNumber;
  String get reservationDate => _reservationDate;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["id"] = _id;
    map["restaurant_id"] = _restaurantId;
    map["user_id"] = _userId;
    map["seat_number"] = _seatNumber;
    map["reservation_date"] = _reservationDate;
    return map;
  }
}