class ReservationHistoryModel {
  String _restaurantName;
  String _reservationDate;

  ReservationHistoryModel(this._restaurantName, this._reservationDate);

  ReservationHistoryModel.fromMap(dynamic obj) {
    this._restaurantName = obj['name'];
    this._reservationDate = obj['reservation_date'];
  }

  String get restaurantName => _restaurantName;
  String get reservationDate => _reservationDate;
}