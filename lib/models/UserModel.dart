class UserModel {
  int id;
  String _username;
  String _password;
  String _nickname;
  String _dob;
  String _gender;
  String _address;
  String _nationality;

  UserModel(this._username,this._password,this._nickname, this._dob,this._gender, this._address,this._nationality);

  UserModel.fromMap(dynamic obj) {
    this.id = obj['id'];
    this._username = obj['username'];
    this._password = obj['password'];
    this._nickname = obj['nickname'];
    this._dob = obj['dob'];
    this._gender = obj['gender'];
    this._address = obj['address'];
    this._nationality = obj['nationality'];
  }

  String get username => _username;
  String get password => _password;
  String get nickname => _nickname;
  String get dob => _dob;
  String get gender => _gender;
  String get address => _address;
  String get nationality => _nationality;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["username"] = _username;
    map["password"] = _password;
    map["nickname"] = _nickname;
    map["dob"] = _dob;
    map["gender"] = _gender;
    map["address"] = _address;
    map["nationality"] = _nationality;
    return map;
  }
}