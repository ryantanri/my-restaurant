class RestaurantModel {
  int id;
  String _name;
  String _rating;
  String _image;
  String _address;
  String _description;
  String _email;
  String _website;
  String _phone;

  RestaurantModel(this._name,this._rating, this._image, this._address, this._description, this._email, this._website, this._phone);

  RestaurantModel.fromMap(dynamic obj) {
    this.id = obj['id'];
    this._name = obj['name'];
    this._rating = obj['rating'];
    this._image = obj['image'];
    this._address = obj['address'];
    this._description = obj['description'];
    this._email = obj['email'];
    this._website = obj['website'];
    this._phone = obj['phone'];
  }

  String get name => _name;
  String get rating => _rating;
  String get image => _image;
  String get address => _address;
  String get description => _description;
  String get email => _email;
  String get website => _website;
  String get phone => _phone;

  Map<String, dynamic> toMap() {
    var map = new Map<String, dynamic>();
    map["name"] = _name;
    map["rating"] = _rating;
    map["image"] = _image;
    map["address"] = _address;
    map["description"] = _description;
    map["email"] = _email;
    map["website"] = _website;
    map["phone"] = _phone;
    return map;
  }
}