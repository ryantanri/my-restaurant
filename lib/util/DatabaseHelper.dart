import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseHelper {
  static final _databaseName = "my_restaurant.db";
  static final _databaseVersion = 1;

  static final DatabaseHelper _instance = new DatabaseHelper.internal();
  factory DatabaseHelper() => _instance;

  // only have a single app-wide reference to the database
  static Database _database;
  Future<Database> get database async {
    if (_database != null) return _database;
    // lazily instantiate the db the first time it is accessed
    _database = await _initDatabase();
    return _database;
  }

  DatabaseHelper.internal();

  // this opens the database (and creates it if it doesn't exist)
  _initDatabase() async {
    String path = join(await getDatabasesPath(), _databaseName);
    return await openDatabase(path,
        version: _databaseVersion,
        onCreate: _onCreate);
  }

  // SQL code to create the database table
  Future _onCreate(Database db, int version) async {
    await db.execute('''
          CREATE TABLE user (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            username TEXT NOT NULL,
            password TEXT NOT NULL,
            nickname TEXT NOT NULL,
            dob TEXT NOT NULL,
            gender TEXT NOT NULL,
            address TEXT NOT NULL,
            nationality TEXT NOT NULL
          )
          ''');
    await db.execute('''
          CREATE TABLE restaurant (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            name TEXT NOT NULL,
            rating TEXT NOT NULL,
            image TEXT NOT NULL,
            address TEXT NOT NULL,
            description TEXT NOT NULL,
            email TEXT NOT NULL,
            website TEXT NOT NULL,
            phone TEXT NOT NULL
          )
          ''');
    await db.execute('''
          CREATE TABLE reservation (
            id TEXT PRIMARY KEY,
            restaurant_id TEXT NOT NULL,
            user_id TEXT NOT NULL,
            seat_number TEXT NOT NULL,
            reservation_date TEXT NOT NULL
          )
          ''');
  }
}