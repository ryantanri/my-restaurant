import 'dart:async';
import 'package:my_restaurant/models/RestaurantModel.dart';
import 'package:my_restaurant/util/DatabaseHelper.dart';


class RestaurantTableController {
  DatabaseHelper con = new DatabaseHelper();
  static const tableName = "restaurant";

//insertion
  Future<int> saveRestaurant(RestaurantModel restaurant) async {
    var dbClient = await con.database;
    int res = await dbClient.insert(tableName, restaurant.toMap());
    return res;
  }

  Future<List<RestaurantModel>> getAllRestaurant() async {
    var dbClient = await con.database;
    var res = await dbClient.query(tableName);
    List<RestaurantModel> list = res.isNotEmpty ? res.map((c) => RestaurantModel.fromMap(c)).toList() : null;
    return list;
  }
}