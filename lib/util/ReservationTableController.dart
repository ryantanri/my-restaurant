import 'dart:async';
import 'package:my_restaurant/models/ReservationHistoryModel.dart';
import 'package:my_restaurant/models/ReservationModel.dart';
import 'package:my_restaurant/util/DatabaseHelper.dart';


class ReservationTableController {
  DatabaseHelper con = new DatabaseHelper();
  static const tableName = "reservation";
  static const restaurantTableName = "restaurant";

//insertion
  Future<int> saveReservation(ReservationModel reservation) async {
    var dbClient = await con.database;
    int res = await dbClient.insert(tableName, reservation.toMap());
    return res;
  }

  Future<List<ReservationHistoryModel>> getAllUserReservation(String userId) async {
    var dbClient = await con.database;
    var res = await dbClient.rawQuery("SELECT $restaurantTableName.name, $tableName.reservation_date FROM $tableName INNER JOIN $restaurantTableName ON $tableName.restaurant_id=$restaurantTableName.id WHERE user_id = '$userId'");
    List<ReservationHistoryModel> list = res.isNotEmpty ? res.map((c) => ReservationHistoryModel.fromMap(c)).toList() : null;
    return list;
  }

  Future<List<ReservationModel>> getAllReservation() async {
    var dbClient = await con.database;
    var res = await dbClient.query(tableName);
    List<ReservationModel> list = res.isNotEmpty ? res.map((c) => ReservationModel.fromMap(c)).toList() : null;
    return list;
  }
}