import 'dart:async';
import 'package:my_restaurant/models/UserModel.dart';
import 'package:my_restaurant/util/DatabaseHelper.dart';


class UserTableController {
  DatabaseHelper con = new DatabaseHelper();
  static const tableName = "user";

//insertion
  Future<int> saveUser(UserModel user) async {
    var dbClient = await con.database;
    int res = await dbClient.insert(tableName, user.toMap());
    return res;
  }

  Future<UserModel> getLogin(String user, String password) async {
    var dbClient = await con.database;
    var res = await dbClient.rawQuery("SELECT * FROM $tableName WHERE username = '$user' and password = '$password'");
    if (res.length > 0) {
      return new UserModel.fromMap(res.first);
    }
    return null;
  }

  Future<List<UserModel>> getAllUser() async {
    var dbClient = await con.database;
    var res = await dbClient.query(tableName);
    List<UserModel> list = res.isNotEmpty ? res.map((c) => UserModel.fromMap(c)).toList() : null;
    return list;
  }
}