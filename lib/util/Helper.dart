import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';


class Helper {
  static String formatDateTimeToString(DateTime date) {
    var formatter = new DateFormat('d-M-yyyy');
    var dateString = formatter.format(date);
    return dateString;
  }

  static showToast(BuildContext context, String msg, int duration) {
    ScaffoldMessenger.of(context).removeCurrentSnackBar();
    final snackBar = SnackBar(
        content: Text(msg),
        duration: Duration(seconds: duration)
    );
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  static ResultCheckRegisterParam checkRegisterParam(RegisterParam param) {
    var value = true;
    var errorType = ErrorType.none;

    RegExp regExpPassword = new RegExp(r'^(?=.*[A-Z])(?=.*\d)[A-Za-z\d\W]{8,32}$',
      caseSensitive: true,
      multiLine: false,
    );
    RegExp regExpAddress = new RegExp(r'.*street$',
      caseSensitive: false,
      multiLine: false,
    );

    if (param.username.characters.length < 3 || param.username.characters.length > 30) {
      value = false;
      errorType = ErrorType.username;
    } else if (!regExpPassword.hasMatch(param.password)) {
      value = false;
      errorType = ErrorType.password;
    } else if (param.password != param.confirmPassword) {
      value = false;
      errorType = ErrorType.confirmPassword;
    } else if (param.nickname.characters.length < 3 || param.nickname.characters.length > 21) {
      value = false;
      errorType = ErrorType.nickname;
    } else if (param.nationality == null || param.nationality == "") {
      value = false;
      errorType = ErrorType.nationality;
    } else if (!regExpAddress.hasMatch(param.address)) {
      value = false;
      errorType = ErrorType.address;
    } else if (param.dob == null || param.dob == "") {
      value = false;
      errorType = ErrorType.dob;
    } else if (param.gender == null || param.gender == "") {
      value = false;
      errorType = ErrorType.gender;
    }

    var result = ResultCheckRegisterParam();
    result.result = value;
    result.errorType = errorType;
    return result;
  }
}

class ResultCheckRegisterParam {
  bool result;
  ErrorType errorType;
}

class RegisterParam {
  String username;
  String password;
  String confirmPassword;
  String nickname;
  String nationality;
  String address;
  String dob;
  String gender;
}

enum ErrorType { username, password, confirmPassword, nickname, nationality, address, dob, gender, none }