import 'package:shared_preferences/shared_preferences.dart';

class Preferences {

  static String keyId = "id";

  static void setId(String id) async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    await pref.setString(keyId, id);
  }

  static Future<String> getId() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return '${pref.getString(keyId)}';
  }
}