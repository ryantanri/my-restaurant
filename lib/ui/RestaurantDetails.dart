import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:intl/intl.dart';
import 'package:my_restaurant/models/ReservationModel.dart';
import 'package:my_restaurant/models/RestaurantModel.dart';
import 'package:my_restaurant/util/Helper.dart';
import 'package:my_restaurant/util/Preferences.dart';
import 'package:my_restaurant/util/ReservationTableController.dart';

class RestaurantDetails extends StatefulWidget {
  RestaurantModel data;

  @override
  _RestaurantDetailsState createState() => _RestaurantDetailsState();

  RestaurantDetails({Key key, @required this.data}) : super(key: key);
}

class _RestaurantDetailsState extends State<RestaurantDetails> {
  RestaurantModel dataRestaurant;
  ReservationTableController reservationController = ReservationTableController();

  @override
  void initState() {
    super.initState();
    dataRestaurant = widget.data;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: Text('${dataRestaurant.name}'),
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.only(left: 8, right: 8),
        scrollDirection: Axis.vertical,
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset("lib/${dataRestaurant.image}", width: 100.0, height:100.0),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text('ID: ${dataRestaurant.id}'),
                        Text('Name: ${dataRestaurant.name}'),
                        Text('Ratings: ${dataRestaurant.rating}')
                      ],
                    ),
                  ),
                )
              ]
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: MediaQuery.of(context).size.width ,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('Address: ${dataRestaurant.address}'),
                    Text('Phone: ${dataRestaurant.phone}'),
                    Text('Email: ${dataRestaurant.email}'),
                    Text('Website: ${dataRestaurant.website}')
                  ],
                ),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              padding: const EdgeInsets.all(8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text('Description :', textAlign: TextAlign.left,),
                  Text('${dataRestaurant.description}')
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Colors.lightBlueAccent[100],
                  borderRadius: BorderRadius.all(Radius.circular(8))
              ),
              child: GestureDetector(
                  onTap: () {
                    doReservationProcess();
                  },
                  child: Text('Reserve Table', textAlign: TextAlign.center)
              ),
            )
          ],
        ),
      ),
    );
  }

  void doReservationProcess() {
    // todo: add reservation process
    NumberFormat formatter = new NumberFormat("BK000");
    var result = reservationController.getAllReservation();
    int maxSeatNumber = 30;
    int seatNumber = Random().nextInt(maxSeatNumber) + 1;
    Preferences.getId().then((userId) {
      result.then((values) {
        final reservationId = formatter.format((values == null ? 0 : values.length) + 1);
        var reservationModel = ReservationModel(reservationId, '${dataRestaurant.id}', userId, '$seatNumber', Helper.formatDateTimeToString(DateTime.now()));
        reservationController.saveReservation(reservationModel).then((reservationValue) {
          Helper.showToast(context, "Reservation is Success", 2);
        });
      });
    });
  }
}
