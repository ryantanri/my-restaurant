import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:my_restaurant/models/UserModel.dart';
import 'package:my_restaurant/util/Helper.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:my_restaurant/util/UserTableController.dart';

class RegisterView extends StatefulWidget {
  @override
  _RegisterViewState createState() => _RegisterViewState();

  RegisterView() : super();
}

class _RegisterViewState extends State<RegisterView> {
  TextEditingController usernameTextEditController = TextEditingController();
  TextEditingController passwordTextEditController = TextEditingController();
  TextEditingController confirmPasswordTextEditController = TextEditingController();
  TextEditingController nicknameTextEditController = TextEditingController();
  TextEditingController dobTextEditController = TextEditingController();
  TextEditingController nationalityTextEditController = TextEditingController();
  TextEditingController addressTextEditController = TextEditingController();
  DateTime selectedDate;
  Gender selectedGender;
  UserTableController controller = UserTableController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: Text('Login'),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(labelText: 'Username'),
                keyboardType: TextInputType.text,
                controller: usernameTextEditController,
                maxLength: 30,
              ),
              TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(labelText: 'Password'),
                  keyboardType: TextInputType.visiblePassword,
                  controller: passwordTextEditController,
                  maxLength: 32
              ),
              TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(labelText: 'Confirm Password'),
                  keyboardType: TextInputType.visiblePassword,
                  controller: confirmPasswordTextEditController,
                  maxLength: 32
              ),
              TextFormField(
                  decoration: InputDecoration(labelText: 'Nickname'),
                  keyboardType: TextInputType.visiblePassword,
                  controller: nicknameTextEditController,
                  maxLength: 21
              ),
              TextFormField(
                  decoration: InputDecoration(labelText: 'Nationality'),
                  keyboardType: TextInputType.visiblePassword,
                  controller: nationalityTextEditController
              ),
              TextFormField(
                  decoration: InputDecoration(labelText: 'Address'),
                  keyboardType: TextInputType.visiblePassword,
                  controller: addressTextEditController
              ),
              TextFormField(
                  decoration: InputDecoration(labelText: 'Date of Birth'),
                  focusNode: AlwaysDisabledFocusNode(),
                  controller: dobTextEditController,
                  onTap: () => openDatePicker()
              ),
              Padding(
                padding: const EdgeInsets.only(top : 16.0),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    'Gender :',
                    style: TextStyle(fontSize: 16),
                  )
                )
              ),
              Column(
                children: <Widget> [
                  ListTile(
                    title: const Text('Male'),
                    leading: Radio(
                      value: Gender.male,
                      groupValue: selectedGender,
                      onChanged: (Gender value) {
                        setState(() {
                          selectedGender = value;
                        });
                      },
                    ),
                  ),
                  ListTile(
                    title: const Text('Female'),
                    leading: Radio(
                      value: Gender.female,
                      groupValue: selectedGender,
                      onChanged: (Gender value) {
                        setState(() {
                          selectedGender = value;
                        });
                      },
                    ),
                  )
                ]
              ),
              Padding(
                padding: EdgeInsets.only(top: 16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: (MediaQuery.of(context).size.width / 2) - 16,
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          color: Colors.lightBlueAccent[100],
                          borderRadius: BorderRadius.all(Radius.circular(8))
                      ),
                      child: GestureDetector(
                          onTap: () {
                            usernameTextEditController.clear();
                            passwordTextEditController.clear();
                          },
                          child: Text('Reset', textAlign: TextAlign.center)
                      ),
                    ),
                    Container(
                      width: (MediaQuery.of(context).size.width / 2) - 16,
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          color: Colors.lightBlueAccent[100],
                          borderRadius: BorderRadius.all(Radius.circular(8))
                      ),
                      child: GestureDetector(
                          onTap: () => doCheckRegistrationParam(),
                          child: Text('Register', textAlign: TextAlign.center)
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void doCheckRegistrationParam() {
    var registerParam = RegisterParam();

    registerParam.username = usernameTextEditController.value.text;
    registerParam.password = passwordTextEditController.value.text;
    registerParam.confirmPassword = confirmPasswordTextEditController.value.text;
    registerParam.nickname = nicknameTextEditController.value.text;
    registerParam.nationality = nationalityTextEditController.value.text;
    registerParam.address = addressTextEditController.value.text;
    registerParam.dob = dobTextEditController.value.text;
    registerParam.gender = selectedGender == Gender.male ? 'male' : selectedGender == Gender.female ? 'female' : '';

    final isParamValid = Helper.checkRegisterParam(registerParam);
    if (isParamValid.result) {
      var isUsernameAlreadyExist = false;
      controller.getAllUser().then((values) => {
        if (values != null) {
          for (var data in values) {
            if (data.username == registerParam.username) {
              isUsernameAlreadyExist = true
            }
          },
          if (!isUsernameAlreadyExist) {
            doRegistrationProcess(registerParam)
          } else
            {
              Helper.showToast(context, "username already exist.", 2)
            }
        } else {
          doRegistrationProcess(registerParam)
        }
      });
    } else {
      switch (isParamValid.errorType) {
        case ErrorType.username: {
          Helper.showToast(context, "username must be between 3 and 30 characters.", 2);
        }
        break;
        case ErrorType.password: {
          Helper.showToast(context, "password must be between 8 and 32 characters, must contain at least 1 Capital letter, and must contain at least 1 Digit.", 2);
        }
        break;
        case ErrorType.confirmPassword: {
          Helper.showToast(context, "password and confirm do not match.", 2);
        }
        break;
        case ErrorType.nickname: {
          Helper.showToast(context, "nick name must be between 3 and 21 characters.", 2);
        }
        break;
        case ErrorType.dob: {
          Helper.showToast(context, "please fill your date of birth.", 2);
        }
        break;
        case ErrorType.gender: {
          Helper.showToast(context, "please choose your gender.", 2);
        }
        break;
        case ErrorType.address: {
          Helper.showToast(context, "address must be ended with 'Street'.", 2);
        }
        break;
        case ErrorType.nationality: {
          Helper.showToast(context, "please fill your nationality.", 2);
        }
        break;
        case ErrorType.none:
          break;
      }
    }
  }

  void doRegistrationProcess(RegisterParam param) {
    final userModel = UserModel(
      param.username,
      param.password,
      param.nickname,
      param.dob,
      param.gender,
      param.address,
      param.nationality
    );
    var result = controller.saveUser(userModel);
    result.then((value) => {
      Navigator.pop(context)
      // ignore: argument_type_not_assignable_catch_error_on_error
    }).catchError((onError) => {
      Helper.showToast(context, "Registration Failed", 2)
    });
  }

  void openDatePicker() {
    setState(() {
      DatePicker.showDatePicker(context,
          showTitleActions: true, locale: LocaleType.en, onChanged: (date) {
            selectedDate = date;
            final dateString = Helper.formatDateTimeToString(date);
          }, onConfirm: (date) {
            setState(() {});
            Future.delayed(Duration(seconds: 0)).then((value) {
              selectedDate = date;
              final dateString = Helper.formatDateTimeToString(date);
              this.dobTextEditController.text = dateString;
            });
          }, currentTime: selectedDate != null ? selectedDate : DateTime.now());
    });
  }
}

enum Gender {male, female}

class AlwaysDisabledFocusNode extends FocusNode {
  @override
  bool get hasFocus => false;
}