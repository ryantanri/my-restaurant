import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:my_restaurant/models/RestaurantModel.dart';
import 'package:my_restaurant/ui/ReservationHistory.dart';
import 'package:my_restaurant/ui/RestaurantDetails.dart';
import 'package:my_restaurant/util/RestaurantTableController.dart';

class RestaurantList extends StatefulWidget {
  @override
  _RestaurantListState createState() => _RestaurantListState();
  RestaurantList() : super();
}

class _RestaurantListState extends State<RestaurantList> {
  RestaurantTableController controller = RestaurantTableController();
  List<RestaurantModel> restaurantList = [];
  final numberOfRestaurant = 3;
  final List<RestaurantModel> restaurantToAdd = [RestaurantModel('Restaurant 1', '4.2', 'asset/01.jpg', 'Address 1 Street', 'Description for Restaurant 1', 'restaurant1@gmail.com', 'restaurant1.com', '55548265'),
                                                 RestaurantModel('Restaurant 2', '3.2', 'asset/02.jpg', 'Address 2 Street', 'Description for Restaurant 2', 'restaurant2@gmail.com', 'restaurant2.com', '55486565'),
                                                 RestaurantModel('Restaurant 3', '4.7', 'asset/03.jpg', 'Address 3 Street', 'Description for Restaurant 3', 'restaurant3@gmail.com', 'restaurant3.com', '53785265')];

  @override
  void initState() {
    super.initState();
    getRestaurantList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: Text('Restaurants'),
          actions: <Widget>[
            PopupMenuButton<String>(
              onSelected: handleClick,
              itemBuilder: (BuildContext context) {
                return {'Reservation History'}.map((String choice) {
                  return PopupMenuItem<String>(
                    value: choice,
                    child: Text(choice),
                  );
                }).toList();
              },
            ),
          ]
      ),
      body:ListView.builder (
          itemCount: restaurantList.length,
          itemBuilder: (BuildContext ctxt, int index) {
            return ListTile(
              leading: Image.asset("lib/${restaurantList[index].image}", width: 100.0, height:100.0),
              title: Text(restaurantList[index].name),
              subtitle: Text("Ratings: ${restaurantList[index].rating}"),
              trailing: Container(
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: Colors.lightBlueAccent[100],
                    borderRadius: BorderRadius.all(Radius.circular(8))
                ),
                child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => RestaurantDetails(data: restaurantList[index])),
                      );
                    },
                    child: Text('Detail', textAlign: TextAlign.center)
                ),
              ),
            );
          })
    );
  }

  void getRestaurantList() {
    var result = controller.getAllRestaurant();
    result.then((values) {
      if ((values == null) || (values.length < numberOfRestaurant)) {
        insertRestaurantData(values == null ? 0 : values.length);
      } else {
        values.forEach((element) {restaurantList.add(element);});
        setState(() {});
      }
    });
  }

  void insertRestaurantData(int totalData) {
    controller.saveRestaurant(restaurantToAdd[totalData]).then((value) {
      getRestaurantList();
    });
  }

  void handleClick(String value) {
    switch (value) {
      case 'Reservation History':
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => ReservationHistory()),
        );
        break;
    }
  }
}