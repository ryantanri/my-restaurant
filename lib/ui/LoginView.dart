

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:my_restaurant/ui/RegisterView.dart';
import 'package:my_restaurant/ui/RestaurantList.dart';
import 'package:my_restaurant/util/Helper.dart';
import 'package:my_restaurant/util/Preferences.dart';
import 'package:my_restaurant/util/UserTableController.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();

  LoginView() : super();
}

class _LoginViewState extends State<LoginView> {
  TextEditingController usernameTextEditController = TextEditingController();
  TextEditingController passwordTextEditController = TextEditingController();
  UserTableController controller = UserTableController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: Text('Login'),
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(labelText: 'Username'),
                keyboardType: TextInputType.text,
                controller: usernameTextEditController,
                maxLength: 30,
              ),
              TextFormField(
                  obscureText: true,
                  decoration: InputDecoration(labelText: 'Password'),
                  keyboardType: TextInputType.visiblePassword,
                  controller: passwordTextEditController,
                  maxLength: 32
              ),
              Padding(
                padding: EdgeInsets.only(top: 8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      width: (MediaQuery.of(context).size.width / 2) - 16,
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                        color: Colors.lightBlueAccent[100],
                        borderRadius: BorderRadius.all(Radius.circular(8))
                      ),
                      child: GestureDetector(
                        onTap: () {
                          usernameTextEditController.clear();
                          passwordTextEditController.clear();
                        },
                        child: Text('Reset', textAlign: TextAlign.center)
                      ),
                    ),
                    Container(
                      width: (MediaQuery.of(context).size.width / 2) - 16,
                      padding: EdgeInsets.all(8),
                      decoration: BoxDecoration(
                          color: Colors.lightBlueAccent[100],
                          borderRadius: BorderRadius.all(Radius.circular(8))
                      ),
                      child: GestureDetector(
                        onTap: () {
                         _doLogin();
                        },
                        child: Text('login', textAlign: TextAlign.center)
                      ),
                    )
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 8),
                width: MediaQuery.of(context).size.width,
                padding: EdgeInsets.all(8),
                decoration: BoxDecoration(
                    color: Colors.lightBlueAccent[100],
                    borderRadius: BorderRadius.all(Radius.circular(8))
                ),
                child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => RegisterView()),
                      );
                    },
                    child: Text('Register', textAlign: TextAlign.center)
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  void _doLogin() {
    if (usernameTextEditController.value.text == "") {
      Helper.showToast(context, "Please fill your username", 2);
    } else if (passwordTextEditController.value.text == "") {
      Helper.showToast(context, "Please fill your password", 2);
    } else {
      var result = controller.getLogin(usernameTextEditController.value.text,
          passwordTextEditController.value.text);
      result.then((value) =>
      {
        Preferences.setId(value.id.toString()),
        Navigator.of(context).pushReplacement(
          new MaterialPageRoute(builder: (context) => RestaurantList()),
        )
        // ignore: argument_type_not_assignable_catch_error_on_error
      }).catchError((onError) =>
      {
        Helper.showToast(context, "User not registered", 2)
      });
    }
  }
}