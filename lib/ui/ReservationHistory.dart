import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:my_restaurant/models/ReservationHistoryModel.dart';
import 'package:my_restaurant/util/Preferences.dart';
import 'package:my_restaurant/util/ReservationTableController.dart';

class ReservationHistory extends StatefulWidget {
  @override
  _ReservationHistoryState createState() => _ReservationHistoryState();
  ReservationHistory() : super();
}

class _ReservationHistoryState extends State<ReservationHistory> {
  ReservationTableController controller = ReservationTableController();
  List<ReservationHistoryModel> reservationList = [];

  @override
  void initState() {
    super.initState();
    getReservationHistory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.blueGrey,
          centerTitle: true,
          title: Text('Reservations'),
        ),
        body:ListView.builder (
            itemCount: reservationList.length,
            itemBuilder: (BuildContext ctxt, int index) {
              return ListTile(
                title: Text(reservationList[index].restaurantName),
                subtitle: Text("${reservationList[index].reservationDate}"),
              );
            })
    );
  }

  void getReservationHistory() {
    Preferences.getId().then((userId) {
      var result = controller.getAllUserReservation(userId);
      result.then((values) => {
        values.forEach((element) {reservationList.add(element);}),
        setState(() {})
      });
    });
  }
}